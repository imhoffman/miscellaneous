//  https://www.ncbi.nlm.nih.gov/IEB/ToolBox/C_DOC/lxr/source/regexp/demo/pcredemo.c
//  https://man7.org/linux/man-pages/man3/pcredemo.3.html

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <pcre.h>
#include <sys/stat.h>

#define OVECCOUNT          3 * 10  /* max number of regex matches; mult of 3 */
#define CHUNK_SIZE         65536   /* size of chunks for binary file read-in */
#define MAX_HEADER_SIZE    1024    /* size of small work array for plain-text regex */
#define MAX_COMMENT_SIZE   80      /* max size of an individual comment */
#define MAX_NUM_COMMENTS   2 * 32  /* max number of individual header comments; pairs */
#define PARSER_ERROR       -99      /* arbitrary semafore value */


//
//  struct of addresses of header values
//   for population by parsing routine(s)
//
struct
header_info_struct
{
  int  *file_format;
  int  *header_size;
  int  *header_ints;
  char *header_comments;
  int  *num_comments;
  int  *header_comments_indices;
};


//
//  file reader
//   `malloc`s the buffer and returns its
//    address; to be freed by the caller
//   reads the file in chunks
//   the file size is also made available
//    to the calling scope
//
uint8_t*
file_reader( const char *filename, const int verbose, long *file_size )
{
  int err;
  long bytes_read, bytes_remaining;
  uint8_t *file_buffer = NULL;
  FILE *fp = fopen( filename, "rb" );

  err = fseek( fp, 0L, SEEK_END );
  if ( err == 0 ) {
    *file_size = ftell( fp );
  } else {
    fprintf( stderr, " error seeking end of file\n" );
  }

  err = fseek( fp, 0L, SEEK_SET );
  if ( err != 0 ) fprintf( stderr, " error rewinding file\n" );

  file_buffer =(uint8_t *) malloc( (size_t)*file_size * sizeof( uint8_t ) );
  if ( file_buffer == NULL ) fprintf( stderr, " error allocating memory\n" );

  bytes_read = 0L;
  bytes_remaining = *file_size;
  if (verbose) fprintf( stdout, "\n beginning chunked read of %ld bytes...\n", *file_size );
  for( long i=0L; i < *file_size; i += (long)CHUNK_SIZE ) {
    if (verbose) fprintf( stdout, " %11ld bytes remaining to be read\n", bytes_remaining );
    if ( bytes_remaining >= (long)CHUNK_SIZE ) {
      bytes_read += fread( &file_buffer[i], (size_t)1L, (size_t)CHUNK_SIZE, fp );
      bytes_remaining = *file_size - bytes_read;
    } else {
      bytes_read += fread( &file_buffer[i], (size_t)1L, (size_t)bytes_remaining, fp );
    }
  }

  fclose( fp );

  //  the calling scope should check for a NULL return
  //   either because the read-in failed or because the
  //   earlier allocation failed
  if ( bytes_read != *file_size ) {
    fprintf( stderr, " error reading file\n" );
    if ( file_buffer != NULL ) free( file_buffer );
    return NULL;
  }

  return file_buffer;
}


//
//  read the magic number from the beginning of the header
//  this could coincidentally yield a legit magic
//   number since it merely reads the first two bytes, but
//   I prefer this to a complete regex at this stage
//
int
parse_magic_number( const char *file_string )
{
  int out=PARSER_ERROR;
  char num_str[3] = { '\0' };
  strncpy( num_str, file_string, 2 );

  if ( (sscanf( num_str, "P%1d", &out )) != 1 )
    fprintf( stderr, " error with magic number scanf\n" );

  return out;
}


//
//  regex parser
//
void
parse_header( const uint8_t *file_contents,
              const int header_buffer_length,
              const int verbose,
	      const struct header_info_struct h )
{
  //  local declarations
  pcre *re;
  const char *error, *re_pattern;
  int erroffset, rc, ovector[OVECCOUNT];
  int dim, ic, total_comments_length;
  char header_buffer[header_buffer_length];
  char comment[MAX_COMMENT_SIZE] = { '\0' };

  //  regex pattern, either two or three ints to capture
  re_pattern =
    "^P\\d\\s+"
    "(#[\\s\\S]*(?:\\r\\n|\\n)(?=\\d+))*"
    "(\\d+)\\s+"
    "(?:(#[\\s\\S]*(?:\\r\\n|\\n)(?=\\d+))*(\\d+)\\s+){0,1}"
    "(#[\\s\\S]*(?:\\r\\n|\\n)(?=\\d+))*"
    "(\\d+)(?:\\r\\n|\\n)";

  //  semafore to the calling scope in case of error
  *(h.file_format) = PARSER_ERROR;

  //  slice a small piece off the top for parsing work
  //   that is no larger than the file, as per calling scope
  memset( header_buffer, '\0', (size_t)header_buffer_length );
  memcpy( header_buffer, file_contents, (size_t)header_buffer_length );
  //strncpy( header_buffer, (char *)file_contents, (size_t)header_buffer_length );
  header_buffer[header_buffer_length-1] = '\0';

  //  obtain magic number separately from regex
  *(h.file_format) = parse_magic_number( header_buffer );
  if ( *(h.file_format) < 1  ||  *(h.file_format) > 6 ) {
    fprintf( stderr, " error reading ppm file format magic number\n" );
    *(h.file_format) = PARSER_ERROR;
    return;
  }
  if (verbose)
    fprintf( stdout, "\n file signature magic bytes: P%d\n", *(h.file_format) );

  //  bitmaps won't have a third dim, so set it to 1 (byte) and mop up later
  if ( *(h.file_format) == 1  ||  *(h.file_format) == 4 ) h.header_ints[2] = 1;

  //  compile the regular expression
  //   this allocates memory at `re`
  re = pcre_compile(
    re_pattern,     /* the string of regex code */
    0,              /* default options */
    &error,         /* for error message */
    &erroffset,     /* for error offset */
    NULL            /* use default character tables */
    );

  //  if the compilation failed:
  //   no memory to free
  //   print the error message and bail out
  if (re == NULL) {
    fprintf( stderr, "PCRE compilation failed at offset %d: %s\n", erroffset, error );
    *(h.file_format) = PARSER_ERROR;
    return;
  }

  //  since the compilation succeeded, run the match
  rc = pcre_exec(
    re,                  /* the compiled pattern */
    NULL,                /* no extra data - we didn't study the pattern */
    header_buffer,       /* the subject string */
    header_buffer_length,/* the length of the subject */
    0,                   /* start at offset 0 in the subject */
    0,                   /* default options */
    ovector,             /* output vector for substring information */
    OVECCOUNT            /* number of elements in the output vector */
    );

  //  check for matching errors
  if (rc < 0) {
    switch(rc) {
      case PCRE_ERROR_NOMATCH:
	if (verbose) { fprintf( stdout, " No regex match\n"); } break;
      default:
	fprintf( stderr, " regex matching error %d\n", rc); break;
    }
    *(h.file_format) = PARSER_ERROR;
    goto matching_failure_error_label;
  }
  //  the match succeded, so proceed

  if (verbose)
    fprintf( stdout, "\n regex match succeeded\n");

  /* perhaps the output vector wasn't big enough;
   * offer a non-fatal diagnostic warning and proceed */
  if (rc == 0) {
    rc = OVECCOUNT/3;
    fprintf( stderr, " ovector only has room for %d captured substrings\n", rc - 1);
  }


  /*
   * parse substrings based on the output regex capture vector
   *  start parsing at index 1 since entire match is at 0
   */
  dim = 0;
  ic  = 0;
  total_comments_length = 0;
  for (int i = 1; i < rc; i++) {
    char *substring_start = header_buffer + ovector[2*i];   //  ptr arith
    int substring_length = ovector[2*i+1] - ovector[2*i];   //  int arith
    int parsed_int;
    char temp_scan[header_buffer_length];

    //  zero these containers each iteration to ensure null-termination
    memset( temp_scan, '\0', (size_t)header_buffer_length );
    memset(   comment, '\0', (size_t)MAX_COMMENT_SIZE );

    if (verbose)
      fprintf( stdout, " %2d: %.*s\n", i, substring_length, substring_start);


    //
    //  obtain integers and store comments from the captures
    //

    //  copy the substring from the regex engine for simpler scanning
    //  mock up a format string for variable-width strings
    strncpy( temp_scan, substring_start, (size_t)substring_length );
    char fmt[6] = { '\0' };
    sprintf( fmt, "%%%dc", substring_length );  //  NB: %c does null-terminate

    //  try to read an integer, else a comment
    if ( (sscanf( temp_scan, "%d", &parsed_int )) == 1 ) {
      h.header_ints[dim] = parsed_int;
      dim++;
      *(h.header_size) = ovector[2*i+1]+1;  // header will end with an int
    }
    else if ( (sscanf( temp_scan, fmt, comment )) == 1 ) {
      strcpy( &(h.header_comments[total_comments_length]), comment );  //  includes null termination
      h.header_comments_indices[2*ic] = total_comments_length;
      total_comments_length += substring_length+2;                 //  null termination plus inc to next entry
      h.header_comments_indices[2*ic+1] = total_comments_length-1;   //  end of comment is the null
      ic++;
    }
  }
  //  assign and adjust returns from counters
  *(h.num_comments) = ic;
  h.header_comments_indices[2* *(h.num_comments)+1]--;


  //
  //  cleanup and return
  //
  matching_failure_error_label:
  //  free memory alloced during the regex compilation
  pcre_free( re );
  return;
}



/*
 *   main program
 */
int
main( int argc, char **argv )
{
  long file_size;
  uint8_t *file_contents = NULL;
  char *file_name, *header_comments = NULL;
  int header_comments_indices[MAX_NUM_COMMENTS], dimensions[3];
  int verbose=0, main_return_val=0, file_format;
  int header_buffer_size, header_size, num_comments=0;
  int height, width, depth;
  struct header_info_struct header_info;

  //
  //  parse cli
  //
  if ( ( argc != 2  &&  argc != 3 ) ||
       ( argc >= 3  &&  strncmp( argv[1], "-v", 2 ) != 0 ) ) {
    fprintf( stdout, " usage: <exe> [-verbose] <ppm file name>\n" );
    return 1;
  }

  if ( argc == 3 ) {
    file_name = argv[2];
    verbose = 1;
    //if ( strncmp( argv[1], "-v", 2 ) == 0 ) verbose = 1;
  } else {
    file_name = argv[1];
  }


  //
  //  read file into buffer
  //
  file_contents = file_reader( file_name, verbose, &file_size );
  if ( file_contents == NULL ) {
    fprintf( stderr, " error during file I/O\n" );
    return 2;
  }

  //  offer diagnostic info
  //   https://stackoverflow.com/a/3555936/11924662
  if ( verbose ) {
    fprintf( stdout, "\n   first three bytes of the file: %02X %02X %02X\n",
	(unsigned)(unsigned char)file_contents[0],
	(unsigned)(unsigned char)file_contents[1],
       	(unsigned)(unsigned char)file_contents[2] );
    fprintf( stdout, " the final two bytes of the file: %02X %02X\n", 
	(unsigned)(unsigned char)file_contents[file_size-2L],
	(unsigned)(unsigned char)file_contents[file_size-1L] );
  }

  //  allocate a buffer for working on the text; modestly large, but
  //   not so large that it runs past the end of the file
  header_buffer_size = MAX_NUM_COMMENTS*MAX_COMMENT_SIZE > (int)file_size ? (int)file_size-1 : MAX_HEADER_SIZE;
  header_comments =(char *) malloc( (size_t)header_buffer_size*sizeof(char) );

  //
  //  load the struct with addresses
  //
  header_info.file_format = &file_format;
  header_info.header_size = &header_size;
  header_info.header_ints = dimensions;
  header_info.header_comments = header_comments;
  header_info.num_comments = &num_comments;
  header_info.header_comments_indices = header_comments_indices;

  //
  //  call the regex parser
  //
  parse_header( file_contents,
                header_buffer_size,
                verbose,
		header_info );

  if ( file_format == PARSER_ERROR ) {
    fprintf( stderr, " FATAL: see preceding errors from parsing/matching\n\n" );
    main_return_val = 3;
    goto header_parse_error_label;
  }

  width  = dimensions[0];
  height = dimensions[1];
  depth  = dimensions[2];

  if (verbose) {
    fprintf( stdout, "\n the image dimensions are %dx%dx%d\n\n", width, height, depth );
    fprintf( stdout, " SEEK_END file size: %ld bytes\n", file_size );
    struct stat st;
    long stat_size;
    stat(file_name, &st);
    stat_size = (long)st.st_size;
    fprintf( stdout, "   stat.h file size: %ld bytes\n", (long)stat_size );
    if ( file_format == 5 || file_format == 6 ) {
      fprintf( stdout, " computed file size: %ld bytes\n",
	  (long)(width*height*depth)/255L*(file_format==6?3L:1L) + (long)header_size );
    } else if ( file_format == 4 ) {
      fprintf( stdout, " computed file size: %ld bytes\n",
	  (long)(width*height+7L)/8L + (long)header_size );
    }
  }

  if (verbose) {
    const int plural = num_comments != 1 ? 1 : 0;
    fprintf( stdout, "\n There %s %d comment%s%s\n", plural?"are":"is", num_comments, plural?"s":"", num_comments==0?".":":" );
    for (int j=0; j<num_comments; j++ )
      fprintf( stdout, "%s\n", &header_comments[ header_comments_indices[2*j] ] );
    fprintf( stdout, "\n" );
  }

  header_parse_error_label:
  free( file_contents );    //  this was malloc'ed in `file_reader`
  free( header_comments );

  return main_return_val;
}


#include<stdio.h>
#include<inttypes.h>

typedef uint64_t u64;


u64
collatz ( const u64 n )
{
  if ( n % (u64)2 == (u64)0 ) {
    return n / (u64)2;
  } else {
    return (u64)3 * n + (u64)1;
  }
}


int
main ( int argc, char* argv[] )
{
  u64 input   =(u64) 0;
  u64 max;
  int counter =      1;

  /*  parse input; read back successful parse  */
  if ( argc < 2 || ( sscanf( argv[1], "%"SCNu64, &input ) != 1 ) ) {
    fprintf( stderr, " usage: ./a.out <integer>\n" );
    return 1;
  }
  fprintf( stdout, "\n      investigating n = %"PRIu64"\n", input );


  /*  loop through returned values until convergence  */
  max = input;
  while ( ( input = collatz( input ) ) != (u64)1 ) {
    if ( input > max ) { max = input; }
    counter++;
  }


  /*  read out results  */
  fprintf(
      stdout,
      " largest value reached: %"PRIu64", steps to converge: %d\n\n",
      max, counter );

  return 0;
}


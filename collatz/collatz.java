// https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/math/BigInteger.html
import java.math.BigInteger;

public class collatz {

  public static void main ( String[] args ) {

    int        counter = 0;
    BigInteger n       = new BigInteger( args[0], 10 );
    BigInteger max_val = n;

    System.out.format( "\n      investigating n = %s\n", n.toString() );

    while ( n.compareTo(BigInteger.ONE) != 0 ) {
      n = next( n );
      counter += 1;
      if ( n.compareTo(max_val) > 0 ) max_val = n;
    }

    System.out.format( " largest value reached: %s, steps to converge: %d\n\n",
	max_val.toString(), counter );

    return;
  }


  static BigInteger next ( BigInteger n ) {
    BigInteger ONE   = new BigInteger( "1", 10 );
    BigInteger TWO   = new BigInteger( "2", 10 );
    BigInteger THREE = new BigInteger( "3", 10 );

    if ( n.mod(TWO).compareTo(BigInteger.ZERO) == 0 ) {
      return n.divide(TWO);
    } else {
      return THREE.multiply(n).add(ONE);
    }
  }

}



 module subs
   implicit none
   integer, parameter :: dw = 38       !! decimal digit width
   integer, parameter :: iw = selected_int_kind( dw )
   contains

   pure function collatz ( n ) result ( m )
     integer ( kind = iw ), intent(in) :: n
     integer ( kind = iw )             :: m

     if ( mod( n, 2_iw ) .eq. 0_iw ) then
       m = n / 2_iw
     else
       m = 3_iw*n + 1_iw
     endif

     return
   end function collatz
 end module subs


 program main
   use subs
   implicit none

   integer (kind=iw) :: inp_val = 0, max_val
   integer           :: counter = 0, errstat
   character(len=dw) :: arg


   call get_command_argument( 1, arg, status=errstat )
   if ( errstat .ne. 0 ) then
     goto 610
   else
     read(arg,*,err=610) inp_val
   endif

   write(6,'(/A,I0)')     '      investigating n = ', inp_val 

   max_val = inp_val
   do while ( inp_val .ne. 1_iw )
     counter = counter + 1
     inp_val = collatz( inp_val )
     if ( inp_val .gt. max_val ) max_val = inp_val
   enddo

   write(6,'(A,I0,A,I0/)') &
     ' largest value reached: ', max_val, &
     ', steps to converge: ', counter

   600 goto 700
   610 continue
     write(0,'(/A,I2,A/)') ' usage: ./a.out <', dw, '-digit integer>'
     call exit(1)
   700 continue
   call exit(0)
 end program main


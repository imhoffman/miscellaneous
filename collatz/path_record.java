// https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/math/BigInteger.html
import java.math.BigInteger;
import java.io.*;

public class path_record {

  public static void main ( String[] args ) throws IOException {

    BigInteger n_orig  = new BigInteger( args[1], 10 );
    BigInteger n       = n_orig;
    BigInteger max_val = n;
    BigInteger THREE   = new BigInteger( "3", 10 );
    BigInteger FOUR    = new BigInteger( "4", 10 );
    BigInteger BARINA  = new BigInteger( "113298124744388651798242538014293435290632", 10 );
    BigInteger MODVAL  = new BigInteger( "10000000000", 10 );

    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter( args[0], true ) );

    //  i.e., while true
    while ( 2 != 1 ) {
      n_orig  = n;
      max_val = n; 
    
      //  log occasional progress report 
      //   but this is really constly to check every time ...
      if ( n.mod(MODVAL).compareTo(BigInteger.ZERO) == 0 ) {
       	bufferedWriter.write(
	    String.format( " investigating n = %s\n", n.toString() ) );
       	bufferedWriter.flush();
      }

      //  pursue convergence and max while useful
      //   nL = (11)_2 as per http://www.ijnc.org/index.php/ijnc/article/view/135
      while ( n.compareTo(n_orig) > 0  &&  n.mod(FOUR) == THREE ) {
        n = next( n );
        if ( n.compareTo(max_val) > 0 ) max_val = n;
      }

      //  write out any winners
      if ( max_val.compareTo(BARINA) > 0 ) {
        bufferedWriter.write( 
	    String.format( " max val of: %s for starting n = %s exceeds Barina's record of %s\n",
	      max_val.toString(),
	      n_orig.toString(),
	      BARINA.toString() ) );
       	bufferedWriter.flush();
      }

      //  update search to next integer
      n = n_orig.add(BigInteger.ONE);
    }

    //  for completeness; `while true` never breaks
    //bufferedWriter.close();
    //return;
  }


  //  the Collatz routine
  static BigInteger next ( BigInteger n ) {
    BigInteger ONE   = new BigInteger( "1", 10 );
    BigInteger TWO   = new BigInteger( "2", 10 );
    BigInteger THREE = new BigInteger( "3", 10 );

    if ( n.mod(TWO).compareTo(BigInteger.ZERO) == 0 ) {
      return n.divide(TWO);
    } else {
      return THREE.multiply(n).add(ONE);
    }
  }

}


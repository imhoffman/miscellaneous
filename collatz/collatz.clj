
(defn collatz [n]
  (if (= 0N (mod n 2N))
    (/ n 2N)
    (+ 1N (* n 3N))))

(let [inp-val (new BigInteger (first *command-line-args*))]
  (println "\n      investigating n =" inp-val)
  (loop [input-value   inp-val
         maximum-value input-value
         counter       1]
    (let [current-value (collatz input-value)
          max-tmp       (if
                          (> current-value maximum-value)
                          current-value maximum-value)]
      (if (not= 1N current-value)
        (recur current-value max-tmp (inc counter))
        (println " largest value reached:" max-tmp
                 "steps to converge:" counter
                 "\n")))))


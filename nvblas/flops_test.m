%
%  adapted from https://wiki.archlinux.org/title/Octave#Performance
%  run as
%   OMP_NUM_THREADS=24 OMP_PLACES=cores OMP_PROC_BIND=spread octave-cli flops_test.m
%  where `24` is the number of available physical cores
%
%  GPU acceleration
%   https://developer.nvidia.com/blog/drop-in-acceleration-gnu-octave/
%   see also notes in nvblas.conf
%
%  how does GPU work scale with `single` and `double` ?
%
%
% [imh@swiftx octave]$ LD_PRELOAD=/usr/local/cuda/targets/x86_64-linux/lib/libnvblas.so NVBLAS_CONFIG_FILE=../octave/nvblas.conf octave-cli flops_test.m 
%[NVBLAS] NVBLAS_CONFIG_FILE environment variable is set to '../octave/nvblas.conf'
%[NVBLAS] Using devices :0 
%[NVBLAS] Config parsed
%GFLOPS = 318.47
%GFLOPS = 318.48
%

%  populate two matrices
N = 8192;
A = single(rand(N, N));
B = single(rand(N, N));

%  start two different timers
start = clock();
tic;

%  execute the multiplication
C = A * B;

%  stop the timers, in reverse order
tic_toc = toc;
elapsedTime = etime(clock(), start);

%  compute operations from known complexity
num_operations = 2*N*N*N - N*N;
GFLOPS = num_operations / elapsedTime / 1.0E+09
GFLOPS = num_operations / tic_toc     / 1.0E+09


//
//  gcc -Wall -I /usr/include/openblas -O3 -fopenmp matmul_sp_flops.c -lopenblas
//

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
//#include <cblas.h>
#include <f77blas.h>
//#include <mkl.h>
//#include <mkl_blas.h>
#include <time.h>
#include <omp.h>


//
//  populate matrix with random values
//
void
init( const long long Nrows,
      const long long Ncols,
      float* matrix )
{
  const int Nthreads = omp_get_max_threads();
  unsigned int seeds[ Nthreads ];

  for( int j=0; j<Nthreads; j++ )
    seeds[j] = rand();

  #pragma omp parallel for \
  default( none ) \
  shared( seeds, matrix, Nrows, Ncols ) \
  collapse( 2 )
  for (long long j = 0LL; j < Ncols; j++)
    for (long long i = 0LL; i < Nrows; i++)
      matrix[j*Nrows + i] = ( (float)rand_r( seeds+(size_t)omp_get_thread_num() ) )/(float)RAND_MAX;

  return;
}
 

//
//  main
//
int
main( int argc, char * argv[] )
{
  //  timing vars
  uint64_t tick, tock;
  struct timespec ts;

  //  consts of this operation
  char transA = 'N', transB = 'N';
  float one = 1.0F, zero = 0.0F;

  int rowsA, colsB, common;
  unsigned long temp;
  float *A, *B, *C;

  //  initialize RNG
  srand( time(NULL) );

  fprintf( stdout, " SINGLE-PRECISION computations using %d of %d threads\n", omp_get_max_threads(), omp_get_num_procs() );

  //  CLI parsing and defaults
  if( argc != 4 ) {
    rowsA = 2; colsB = 4; common = 6;
    fprintf( stdout, " Using defaults: %dx%d, %dx%d\n", rowsA, common, common, colsB );
  } else {
    if( 0 != sscanf( argv[1], "%lu", &temp ) ) { rowsA  =(int) temp; } else { goto parser_fail; }
    if( 0 != sscanf( argv[2], "%lu", &temp ) ) { common =(int) temp; } else { goto parser_fail; }
    if( 0 != sscanf( argv[3], "%lu", &temp ) ) { colsB  =(int) temp; } else { goto parser_fail; }
    fprintf( stdout, " Using user dimensions: %dx%d, %dx%d\n", rowsA, common, common, colsB );
  }

  //  dynamic allocations
  A =(float *) malloc( sizeof(float) * rowsA * common ); 
  B =(float *) malloc( sizeof(float) * colsB * common ); 
  C =(float *) malloc( sizeof(float) * rowsA * colsB ); 


  //
  //  matrix initialization, with timing
  //
  if( 0 == clock_gettime(CLOCK_MONOTONIC_RAW, &ts) ) {
    tick = (uint64_t)ts.tv_sec*1000000000 + (uint64_t)ts.tv_nsec;
  } else { goto time_fail; }

  init( (long long)rowsA,  (long long)common, A );
  init( (long long)common, (long long)colsB,  B );

  if( 0 == clock_gettime(CLOCK_MONOTONIC_RAW, &ts) ) {
    tock = (uint64_t)ts.tv_sec*1000000000 + (uint64_t)ts.tv_nsec;
  } else { goto time_fail; }

  fprintf( stdout, " random initialization took %g seconds\n", ((double)(tock-tick))/1.0E+09 );


  //
  //  matrix multiplication, with timing
  //
  if( 0 == clock_gettime(CLOCK_MONOTONIC_RAW, &ts) ) {
    tick = (uint64_t)ts.tv_sec*1000000000 + (uint64_t)ts.tv_nsec;
  } else { goto time_fail; }

  //cblas_sgemm(&transA, &transB, &rowsA, &colsB, &common, &one, A, &rowsA, B, &common, &zero, C, &rowsA);
  //sgemm(&transA, &transB, &rowsA, &colsB, &common, &one, A, &rowsA, B, &common, &zero, C, &rowsA);
  sgemm_(&transA, &transB, &rowsA, &colsB, &common, &one, A, &rowsA, B, &common, &zero, C, &rowsA);

  if( 0 == clock_gettime(CLOCK_MONOTONIC_RAW, &ts) ) {
    tock = (uint64_t)ts.tv_sec*1000000000 + (uint64_t)ts.tv_nsec;
  } else { goto time_fail; }

  const double elapsed_time = ((double)(tock-tick))/1.0E+09;

  fprintf( stdout, " matrix multiplication took %g seconds\n", elapsed_time );
  fprintf( stdout, " %g GFLOPS\n", (double)rowsA * (double)colsB
		                   *( (double)common + ( (double)common - 1.0 ) )
				   / elapsed_time 
	                           / 1.0E+09 );


  //  clean up
  free( A ); free( B ); free( C );

  //  normal termination
  return 0;

  //
  //  error returns
  //
  parser_fail:
  fprintf( stderr, " command-line parsing failure\n" );
  return 1;

  time_fail:
  free( A ); free( B ); free( C );
  fprintf( stderr, " timing functionality failure\n" );
  return 2;
}


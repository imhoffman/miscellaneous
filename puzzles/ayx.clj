
(require '[clojure.string :as str])
;(require '[clojure.set :as set])


;;
;;  assemble dictionary with key,val of report,manager
;;
(def input-dict
  (->> "ques2.csv"
       slurp
       (#(str/split % #"\n") ,,)
       (map #(re-seq #"\d+" %) ,,)
       (map #(vector (Integer/parseInt (first %)) (Integer/parseInt (second %))) ,,)
       (into {} ,,)))


;;
;;  compute the number of employees on the nth layer
;;   accepts the input dictionary as an argument
;;   the CEO is on the zeroth layer
;;
(defn layer-population [d n]
  ;;
  ;;  rebuild input dictionary as key,val of manager,reports-list
  ;;
  (let [repd (reduce
               #(let [[employee manager] %2]
                  (assoc %1 manager
                         (if (contains? %1 manager)
                           (conj (%1 manager) employee)
                           (list employee))))
               {} d)]
    ;;
    ;;  loop through new dictionary up to the requested layer
    ;;
    (loop [layer (repd 9999)      ;;  in the csv, the CEO reports to `9999`
           m 0]
      (if (= m n)
        (count layer)
        (recur
          (reduce #(apply conj %1 (or (repd %2) (list))) (list) layer)
          (inc m))))))


;;
;;  main
;;
(loop [n 0]
  (let [output (layer-population input-dict n)]
    (when (> output 0)
      (do
        (println " layer" n "has" output (if (= 1 output) "employee" "employees"))
        (recur (inc n))))))



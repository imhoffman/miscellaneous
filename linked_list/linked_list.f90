
module types
  implicit none

  !private
  !public list_node

  !!
  !!  in this implementation, a list is not a special
  !!   structure, nor does it own the operations in
  !!   `subs` as type-bound "methods"; rather a list
  !!   variable is simply a pointer to the head and
  !!   the procedures traverse from there, as needed
  !!
  type list_node
    integer :: val
    type(list_node), pointer :: next_node_addr
  end type list_node

end module types


module subs
  use types
  implicit none

  contains
    subroutine free_list_memory( head_node )
      class(list_node), pointer, intent(inout) :: head_node
      class(list_node), pointer :: temp1, temp2

      temp1 => head_node
      do while ( associated( temp1 ) )
        temp2 => temp1%next_node_addr
        deallocate( temp1 )
        temp1 => temp2
      end do
      return
    end subroutine


    subroutine print_list( head_node )
      class(list_node), pointer, intent(in) :: head_node
      class(list_node), pointer :: next_addr

      nullify( next_addr )
      next_addr => head_node

      write(6,'(/A,1X)',advance="no") "("
      do while ( associated( next_addr ) )
        write(6,'(I0,A)',advance="no") next_addr%val, ","
        next_addr => next_addr%next_node_addr
      end do
      write(6,'(A/)') ")"

      return
    end subroutine


    subroutine push( head_node, new_val )
      class(list_node), pointer, intent(inout) :: head_node
      integer, intent(in)                      :: new_val

      class(list_node), pointer :: new_node
      allocate( new_node )

      new_node%next_node_addr => head_node
      new_node%val = new_val
      head_node => new_node
      return
    end subroutine push


    function pop( head_node ) result( return_val )
      class(list_node), pointer, intent(inout) :: head_node
      integer :: return_val
      class(list_node), pointer :: temp

      return_val = head_node%val
      temp => head_node

      head_node => temp%next_node_addr
      deallocate( temp )

      return
    end function pop


    subroutine append( head_node, new_val )
      class(list_node), pointer, intent(inout) :: head_node
      integer, intent(in)                      :: new_val

      class(list_node), pointer :: temp
      class(list_node), pointer :: new_node
      allocate( new_node )

      temp => head_node%next_node_addr
      do while ( associated( temp%next_node_addr ) )
        temp => temp%next_node_addr
      end do

      temp%next_node_addr => new_node
      new_node%val = new_val
      nullify( new_node%next_node_addr )

      return
    end subroutine


    subroutine insert( head_node, new_val, at_index )
      class(list_node), pointer, intent(inout) :: head_node
      integer, intent(in) :: new_val, at_index

      integer :: counter = 1
      class(list_node), pointer :: temp
      class(list_node), pointer :: new_node
      allocate( new_node )

      !!
      !!  use `push` to add to the head
      !!
      if ( at_index .le. 1 ) then
        deallocate( new_node )
        goto 100
      end if

      !!
      !!  traverse the list; if the desired index
      !!   is off the end, put the new node at the end
      !!
      temp => head_node
      do while ( counter .lt. at_index-1 .and. &
                & associated( temp%next_node_addr ) )
        temp => temp%next_node_addr
        counter = counter + 1
      end do

      new_node%val = new_val
      new_node%next_node_addr => temp%next_node_addr
      temp%next_node_addr => new_node

      100 continue
      return
    end subroutine

end module subs


!!
!!  main program
!!
program main
  use types
  use subs
  implicit none

  !!
  !!  declare, allocate, and assign the initial head the
  !!   same way as all other nodes, in part so that the
  !!   deallocation routine can address them all the same
  !!
  class(list_node), pointer :: L
  !type(list_node), pointer :: L      !!  does not satisfy polymorphism requirements
  allocate( L )

  L%val = 2
  nullify( L%next_node_addr )

  call print_list( L )

  call push( L, 9 )
  call push( L, 4 )
  call push( L, 7 )
  call push( L, 8 )

  call print_list( L )

  write( 6, '(A,1X,I0)' ) " popped the value:", pop( L )
  call print_list( L )

  write( 6, '(A)' ) " insert 18 at one-indexed position 2"
  call insert( L, 18, 2 )
  call print_list( L )

  write( 6, '(A)' ) " append 3"
  call append( L, 3 )
  call print_list( L )
  
  write( 6, '(A)' ) " append the pop"
  call append( L, pop( L ) )
  call print_list( L )
  
  write( 6, '(A)' ) " insert 13 at one-indexed position 3"
  call insert( L, 13, 3 )
  call print_list( L )
  
  call free_list_memory( L )

end program main


#include <stdio.h>
#include <stdlib.h>


//
//  a single node
//
//   the `value` field is `int` here, but could be anything
//   if `int` changes, then so does the type of `pop` and
//    the guts of `print_list`, etc.
//
struct
int_node
{
  int value;
  struct int_node * next_node_addr;
};



//
//  the list contains the addresses of the dynamically
//   allocated nodes, so the list is walked and each
//   address is freed
//  O(n)
//
void
free_list_memory ( struct int_node * head_addr )
{
  struct int_node *temp = NULL;
  struct int_node *next_addr = head_addr;

  while ( next_addr != NULL ) {
    temp = next_addr->next_node_addr;
    free( next_addr );
    next_addr = temp;
  }

  return;
}



//
//  walk the list and print its values,
//   without altering the list
//  O(n)
//  pretty-prints parens, commas, and spaces
//
void
print_list ( const struct int_node * head_addr )
{
  struct int_node *next_addr =(struct int_node *) head_addr;

  fprintf( stdout, "(" );
  while ( next_addr != NULL ) {
    const int output_value = next_addr->value;
    next_addr = next_addr->next_node_addr;
    fprintf( stdout, " %d%s", output_value, next_addr==NULL?" ":"," );
  }
  fprintf( stdout, ")\n" );

  return;
}



//
//  add to the head of the list, which requires passing
//   a pointer to a pointer to the head, so that the
//   changes persist upon return
//  O(1)
//
void
push ( struct int_node ** head_addr_addr, const int new_value )
{
  //  allocate the noew node
  struct int_node *new_head_addr =(struct int_node *) malloc( sizeof( struct int_node ) );

  //  populate the new node
  new_head_addr->value = new_value;
  new_head_addr->next_node_addr = *head_addr_addr;

  //  point the head of the list to the new node
  *head_addr_addr = new_head_addr;

  return;
}



//
//  remove the head node and return its value
//  again, requiring a pointer to a pointer
//  the popped node's memory must be freed
//   now while its address is known, or else
//    it will dangle
//  O(1)
//
int
pop ( struct int_node ** head_addr_addr )
{
  //  get the address of the node to be popped
  void *old_head_addr =(void *) *head_addr_addr;

  //  get the value to be popped
  const int return_value = (*head_addr_addr)->value;

  //  update the head of the list
  *head_addr_addr = (*head_addr_addr)->next_node_addr;

  //  deallocate the popped node
  free( old_head_addr );

  return return_value;
}



//
//   add to the end of the list
//    this is O(n) because the list must be walked
//     in order to find the end
//    but since the head is not changed, address is
//     not needed
//
void
append ( const struct int_node * head_addr, const int new_value )
{
  //  allocate the new node
  struct int_node *new_node_addr =(struct int_node *) malloc( sizeof( struct int_node ) );

  struct int_node *search_temp =(struct int_node *) head_addr;
  struct int_node *tail_addr;

  //
  //  find the end of the list
  //
  while ( search_temp != NULL ) {
    tail_addr = search_temp;
    search_temp = search_temp->next_node_addr;
  }

  //  update the former tail to point to the new node
  tail_addr->next_node_addr = new_node_addr;

  //  populate the new node
  new_node_addr->value = new_value;
  new_node_addr->next_node_addr = NULL;

  return;
}



//
//  insert a node at an arbitrary position
//  if `pos` is greater than the length of the
//   list, the new node goes at the end
//
void
insert ( const struct int_node * head_addr, const int new_value, const int pos )
{
  if ( pos == 0 ) return;    //  use `push` instead

  //  allocate the new node
  struct int_node *new_node_addr =(struct int_node *) malloc( sizeof( struct int_node ) );
  new_node_addr->next_node_addr = NULL;

  struct int_node *before_addr =(struct int_node *) head_addr;
  struct int_node *temp = NULL;
  int c = 1;

  //
  //  find `pos` in the list
  //
  while ( before_addr->next_node_addr != NULL && c < pos ) {
    before_addr = before_addr->next_node_addr;
    c += 1;
  }

  //  update the preceding node to point to the new node
  temp = before_addr->next_node_addr;
  before_addr->next_node_addr = new_node_addr;

  //  populate the new node
  new_node_addr->value = new_value;
  if ( c == pos )
    new_node_addr->next_node_addr = temp;

  return;
}



//
//  main program
//
//   execute a variety of operations, printing the
//    list after each one, as a diagnostic
//
int
main ( void )
{

  //  dynamically allocate the first node, so that it is like all of
  //   the others when it comes time to free it along with any
  //   subsequent allocations that have been made
  struct int_node *L =(struct int_node *) malloc( sizeof( struct int_node ) );

  //  populate the head node
  //  the utilities don't check for an empty list
  L->value = 2;
  L->next_node_addr = NULL;

  fprintf( stdout, "\n after initialization:\n" );
  print_list( L );

  push( &L, 9 );
  push( &L, 4 );
  push( &L, 7 );
  push( &L, 8 );

  fprintf( stdout, "\n after pushing 9 then 4 then 7 then 8:\n" );
  print_list( L );

  fprintf( stdout, "\n popping yields a %d\n", pop( &L ) );

  fprintf( stdout, "\n after popping:\n" );
  print_list( L );

  insert( L, 18, 2 );

  fprintf( stdout, "\n after inserting 18 at zero-indexed position 2:\n" );
  print_list( L );

  append( L, 3 );

  fprintf( stdout, "\n after appending a 3:\n" );
  print_list( L );

  const int popped = pop( &L );
  append( L, popped );
  fprintf( stdout, "\n after appending a pop:\n" );
  print_list( L );

  insert( L, 13, 3 );

  fprintf( stdout, "\n after inserting 13 at zero-indexed position 3:\n" );
  print_list( L );

  fprintf( stdout, "\n" );

  //  clean up
  free_list_memory( L );
  return 0;
}


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>
#include<math.h>
#include<time.h>
#include<sys/types.h>
#include<unistd.h>
#include<omp.h>

#define N_LAYERS 5
#define N_LAYER_IN 2
#define N_LAYER_HIDDEN_1 4
#define N_LAYER_HIDDEN_2 6 
#define N_LAYER_HIDDEN_3 3
#define N_LAYER_OUT 1

#define RAND_PEAK_TO_PEAK 10.0

//
//  a single neuron
//
typedef struct
{
  int n_inputs;
  double *weights;
  double bias;
  int func_id;
} neuron_t;

//
//  allocation and destruction routines for a neuron
//
neuron_t
new_neuron( const int n_inputs, const int func_id )
{
  double *weights =(double *) malloc( (size_t)n_inputs * sizeof(double) );

  const double rand_amp = RAND_PEAK_TO_PEAK;

  for( int j=0; j<n_inputs; j++ )
    weights[j] = rand_amp * (double)rand() / (double)RAND_MAX - rand_amp/2.0;
    //weights[j] = 1.0;

  const double default_bias = rand_amp * (double)rand() / (double)RAND_MAX - rand_amp/2.0;
  //const int default_bias = 0.50;

  neuron_t out = { n_inputs, weights, default_bias, func_id };

  return out;
}

void
free_neuron( neuron_t n ) {
  if( n.weights != NULL ) free( n.weights );
  return;
}



//
//  accepts an int id for func choice
//
double
activation( const int n,
            const double *const inputs,
            const double *const weights,
            const double bias,
            const int func_id )
{
  double arg = 0.0;
  double out = 0.0;

  for( int j=0; j<n; j++ ) {
    //fprintf( stdout, " input %f times weight %f\n", inputs[j], weights[j] );
    arg += inputs[j] * weights[j];
  }

  //fprintf( stdout, " plus bias %f\n", bias );
  arg += bias;

  switch( func_id ){
    case 0:  //  sigmoid
      out = 1.0 / ( 1.0 + exp( -arg ) );
      break;
    case 1:  //  tanh
      out = tanh( arg );
      break;
    case 2:  //  relu
      if( arg <= 0.0 ) {
        out = 0.0;
      } else {
        out = arg;
      }
      break;
    case 3:  //  leaky relu
      if( arg <= 0.0 ) {
        out = arg*0.01;
      } else {
        out = arg;
      }
      break;
    case 4:  //  linear pass-through
      out = arg;
      break;
    default:
      fprintf( stderr, " problem with func_id %d\n", func_id );
  }

  //fprintf( stdout, " for output %f\n", out );
  return out;
}


//
//  for XOR training
//
double
cost(
  const double input[N_LAYER_IN],
  const double output[N_LAYER_OUT]
  )
{
  double answer;

  if( input[0] == input[1] ) {
    answer = 0.0;
  } else {
    answer = 1.0;
  }

  return ( answer - output[0] )*( answer - output[0] );
}

//
//  the function that is the network
//
void
network(
    const int n_layers,
    const int neurons_per_layer[const n_layers],
    const neuron_t *const neurons[n_layers],
    const double *const input,
    double * output
    )
{
  //
  //  allocate/free hidden layers for use here
  //  index input and output for looping
  //
  double *neuron_outputs[n_layers+1];
  for( int j=0; j<n_layers+1; j++ )
    if( j == 0 ) {
      //fprintf( stdout, " layer %d are the inputs' inputs\n", j );
      neuron_outputs[j] =(double *) input;
    } else if( j==n_layers ) {
      //fprintf( stdout, " layer %d are the %d outputs of the network\n", j, neurons_per_layer[j-1] );
      neuron_outputs[j] = output;
    } else {
      //fprintf( stdout, " allocating %d for layer %d\n", neurons_per_layer[j-1], j );
      neuron_outputs[j] =(double *) malloc( (size_t)neurons_per_layer[j-1] * sizeof(double) );
    }

  //
  //  loop over re-indexed layers and neurons
  //
  for( int layer_index=1; layer_index<n_layers+1; layer_index++ )
    for( int neuron=0; neuron<neurons_per_layer[layer_index-1]; neuron++ )
      if( layer_index == 1 ) {
      neuron_outputs[layer_index][neuron] = activation(
          neurons[layer_index-1][neuron].n_inputs,
          &neuron_outputs[layer_index-1][neuron],    //  this is the only difference in the `if`
          neurons[layer_index-1][neuron].weights,
          neurons[layer_index-1][neuron].bias,
          neurons[layer_index-1][neuron].func_id );
      } else {
      neuron_outputs[layer_index][neuron] = activation(
          neurons[layer_index-1][neuron].n_inputs,
          neuron_outputs[layer_index-1],
          neurons[layer_index-1][neuron].weights,
          neurons[layer_index-1][neuron].bias,
          neurons[layer_index-1][neuron].func_id );
      }

  //  free hidden layers' memory
  for( int j=1; j<n_layers; j++ )
    free( neuron_outputs[j] );

  return;
}


//
//  main
//
int
main( int argc, char *argv[] )
{
  //  rand initializer
  pid_t getpid(void);
  srand( time(NULL) + getpid() );
  //srand( 42 );

  //
  //  CLI
  //
  int read = 0;
  double tol;
  if( argc > 1 )
    read = sscanf( argv[1], "%lf", &tol );
  if( read == 0  ||  argc == 1 ) tol = 0.100;
  fprintf( stdout, " stopping tolerance: %0.3f\n", tol );


  //
  //  declare and initialize the neurons
  //
  neuron_t layer1[N_LAYER_IN];
  neuron_t layer2[N_LAYER_HIDDEN_1];
  neuron_t layer3[N_LAYER_HIDDEN_2];
  neuron_t layer4[N_LAYER_HIDDEN_3];
  neuron_t layer5[N_LAYER_OUT];
  
  for( int j=0; j<N_LAYER_IN; j++ )
    layer1[j] = new_neuron( 1, 0 );     //  each input neuron has a single input

  for( int j=0; j<N_LAYER_HIDDEN_1; j++ )
    layer2[j] = new_neuron( N_LAYER_IN, 1 );

  for( int j=0; j<N_LAYER_HIDDEN_2; j++ )
    layer3[j] = new_neuron( N_LAYER_HIDDEN_1, 1 );

  for( int j=0; j<N_LAYER_HIDDEN_3; j++ )
    layer4[j] = new_neuron( N_LAYER_HIDDEN_2, 1 );

  for( int j=0; j<N_LAYER_OUT; j++ )
    layer5[j] = new_neuron( N_LAYER_HIDDEN_3, 3 );

  //
  //  train the network
  //
  const double inputs[4][N_LAYER_IN] = { {0.0,0.0}, {1.0,0.0}, {0.0,1.0}, {1.0,1.0} };
  const int neurons_per_layer[N_LAYERS] = { N_LAYER_IN, N_LAYER_HIDDEN_1, N_LAYER_HIDDEN_2, N_LAYER_HIDDEN_3, N_LAYER_OUT };
  const neuron_t *const layers[N_LAYERS] = { layer1, layer2, layer3, layer4, layer5 };
  double output[N_LAYER_OUT] = { (double)rand()/(double)RAND_MAX };

  int count = 0;
  double score;
  double best = 1.0E+12;

  //
  //  mindless bogo search
  //
  /*
  #pragma omp parallel \
          default( none ) \
          shared( tol, inputs, neurons_per_layer, layers ) \
          private( score ) \
          firstprivate( output )
          */
  while( 1 ) {
    score = 0.0;
    count++;

    //  get rid of the old network
    for( int j=0; j<N_LAYER_IN; j++ )       free_neuron( layer1[j] );
    for( int j=0; j<N_LAYER_HIDDEN_1; j++ ) free_neuron( layer2[j] );
    for( int j=0; j<N_LAYER_HIDDEN_2; j++ ) free_neuron( layer3[j] );
    for( int j=0; j<N_LAYER_HIDDEN_3; j++ ) free_neuron( layer4[j] );
    for( int j=0; j<N_LAYER_OUT; j++ )      free_neuron( layer5[j] );

    //  reallocate a new network
    for( int j=0; j<N_LAYER_IN; j++ )       layer1[j] = new_neuron( 1, 0 );
    for( int j=0; j<N_LAYER_HIDDEN_1; j++ ) layer2[j] = new_neuron( N_LAYER_IN, 1 );
    for( int j=0; j<N_LAYER_HIDDEN_2; j++ ) layer3[j] = new_neuron( N_LAYER_HIDDEN_1, 1 );
    for( int j=0; j<N_LAYER_HIDDEN_3; j++ ) layer4[j] = new_neuron( N_LAYER_HIDDEN_2, 1 );
    for( int j=0; j<N_LAYER_OUT; j++ )      layer5[j] = new_neuron( N_LAYER_HIDDEN_3, 3 );

    //  check the four entries in the truth table
    for( int j=0; j<4; j++ ) {
      double input[N_LAYER_IN];
      for( int k=0; k<2; k++ )
        input[k] = inputs[j][k];
      network( N_LAYERS, neurons_per_layer, layers, input, output );
      //fprintf( stdout, "%f\n", output[0] );
      score += cost( input, output );
    }

    if( score < best ) best = score;

    //  progress print
    if( count % 100000 == 0 ) {
      //fprintf( stdout, "." );
      //fflush(stdout);
      fprintf( stdout, " best: %g\n", best );
    }

    //  tol is set by the CLI parser
    if( score < tol ) break;
  }


  //
  //  query the network
  //
  fprintf( stdout, "\n after %d random attempts:\n", count );
  for( int j=0; j<4; j++ ) {
    double input[N_LAYER_IN];
    for( int k=0; k<2; k++ )
      input[k] = inputs[j][k];
    network( N_LAYERS, neurons_per_layer, layers, input, output );
    fprintf( stdout, " %d XOR %d is ", (int)round(input[0]), (int)round(input[1]) );
    fprintf( stdout, "%d\n", (int)round( output[0] ) );
    //fprintf( stdout, "which has a cost of %f\n", cost( input, output ) );
  }



  //
  //  free memory
  //
  for( int j=0; j<N_LAYER_IN; j++ ) free_neuron( layer1[j] );
  for( int j=0; j<N_LAYER_HIDDEN_1; j++ ) free_neuron( layer2[j] );
  for( int j=0; j<N_LAYER_HIDDEN_2; j++ ) free_neuron( layer3[j] );
  for( int j=0; j<N_LAYER_HIDDEN_3; j++ ) free_neuron( layer4[j] );
  for( int j=0; j<N_LAYER_OUT; j++ ) free_neuron( layer5[j] );

  return 0;
}



